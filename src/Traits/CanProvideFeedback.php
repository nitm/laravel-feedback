<?php

namespace Nitm\Feedback\Traits;


trait CanProvideFeedback
{
    /**
     * Check if a feedback for a specific model needs to be approved.
     * @param mixed $model
     * @return bool
     */
    public function needsFeedbackApproval($model): bool
    {
        return true;
    }

    /**
     * Return all feedback for this model.
     *
     * @return MorphMany
     */
    public function feedbackGiven()
    {
        return $this->morphMany(config('feedback.feedback_class'), 'reportable');
    }

    /**
     * Attach a feedback to this model.
     *
     * @param string $feedback
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function leaveFeedback(string $feedback, string $type = null)
    {
        return $this->leaveFeedbackAsUser(auth()->user(), $feedback, $type);
    }

    /**
     * Attach a feedback to this model as a specific user.
     *
     * @param Model|null $user
     * @param string $feedback
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function leaveFeedbackAsUser(?Model $user, string $feedback, string $type = null)
    {
        $feedbackClass = config('feedback.feedback_class');

        $feedback = new $feedbackClass([
            'content' => $feedback,
            'type' => $type ?? Feedback::FLAG_DEFAULT,
            'user_id' => is_null($user) ? null : $user->getKey(),
            'reportable_id' => $this->getKey(),
            'reportable_type' => get_class(),
        ]);

        return $this->feedback()->save($feedback);
    }
}