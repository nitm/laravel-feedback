# Add feedback to your Laravel application

[![Latest Version on Packagist](https://img.shields.io/packagist/v/nitm/laravel-feedback.svg?style=flat-square)](https://packagist.org/packages/nitm/laravel-feedback)
[![Build Status](https://img.shields.io/travis/nitm/laravel-feedback/master.svg?style=flat-square)](https://travis-ci.org/nitm/laravel-feedback)
[![Quality Score](https://img.shields.io/scrutinizer/g/nitm/laravel-feedback.svg?style=flat-square)](https://scrutinizer-ci.com/g/nitm/laravel-feedback)
[![Total Downloads](https://img.shields.io/packagist/dt/nitm/laravel-feedback.svg?style=flat-square)](https://packagist.org/packages/nitm/laravel-feedback)

Add the ability to associate feedback to your Laravel Eloquent models. The feedback can be nested.

```php
$post = Post::find(1);

$post->leaveFeedback('This is feedback');

$post->leaveFeedbackAsUser($user, 'This is feedback from someone else');
```

## Installation

You can install the package via composer:

```bash
composer require nitm/laravel-feedback
```

The package will automatically register itself.

You can publish the migration with:

```bash
php artisan vendor:publish --provider="Nitm\Feedback\FeedbackServiceProvider" --tag="migrations"
```

After the migration has been published you can create the media-table by running the migrations:

```bash
php artisan migrate
```

You can publish the config-file with:

```bash
php artisan vendor:publish --provider="Nitm\Feedback\FeedbackServiceProvider" --tag="config"
```

## Usage

### Registering Models

To let your models be able to receive feedback, add the `HasFeedback` trait to the model classes.

``` php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nitm\Feedback\Traits\HasFeedback;

class Post extends Model
{
    use HasFeedback;
    ...
}
```

### Creating Feedback

To create feedback on your reportable models, you can use the `comment` method. It receives the string of the comment that you want to store.

```php
$post = Post::find(1);

$comment = $post->leaveFeedback('This is feedback from a user.');
```

The comment method returns the newly created comment class.

Sometimes you also might want to create feedback on behalf of other users. You can do this using the `commentAsUser` method and pass in your user model that should get associated
with this comment:

```php
$post = Post::find(1);

$comment = $post->leaveFeedbackAsUser($yourUser, 'This is feedback from someone else.');
```

### Retrieving Feedback

The models that use the `HasFeedback` trait have access to it's feedback using the `feedback` relation:

```php

$post = Post::find(1);

// Retrieve all feedback
$feedback = $post->feedback;

// Retrieve only approved feedback
$approved = $post->feedback()->approved()->get();

```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email marcel@beyondco.de instead of using the issue tracker.

## Credits

- Inspiration: [Marcel Pociot](https://github.com/mpociot)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
