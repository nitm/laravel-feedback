<?php

namespace Nitm\Feedback;

use Illuminate\Support\ServiceProvider;

class FeedbackServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('feedback.php'),
            ], 'config');


            if (!class_exists('CreateFeedbackTable')) {
                $this->publishes([
                    __DIR__ . '/../database/migrations/create_feedback_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_feedback_table.php'),
                ], 'migrations');
            }
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'feedback');
    }
}