<?php

return [

    /*
     * The feedback class that should be used to store and retrieve
     * the feedback.
     */
    'feedback_class' => \Nitm\Feedback\Feedback::class,

    /*
     * The user model that should be used when associating feedback with
     * feedbackators. If null, the default user provider from your
     * Laravel authentication configuration will be used.
     */
    'user_model' => null,

    // Supports approval. Enable to support approval functionality on backend
    'supports-approval' => false,

    /**
     * Use uuid as primary key.
     */
    'uuids' => false,

    /*
     * User tables foreign key name.
     */
    'user_foreign_key' => 'user_id',

    /*
     * Table name for reported records.
     */
    'table' => 'feedback',
];