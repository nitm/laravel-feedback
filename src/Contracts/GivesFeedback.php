<?php

namespace BeyondCode\Feedback\Contracts;


interface GivesFeedback
{
    /**
     * Check if a comment for a specific model needs to be approved.
     * @param mixed $model
     * @return bool
     */
    public function needsFeedbackApproval($model): bool;
}