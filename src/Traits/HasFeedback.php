<?php

namespace Nitm\Feedback\Traits;


use Nitm\Feedback\Feedback;
use Illuminate\Database\Eloquent\Model;
use Nitm\Feedback\Contracts\GivesFeedback;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasFeedback
{
    /**
     * @param \Illuminate\Database\Eloquent\Model $user
     *
     * @return bool
     */
    public function isReportedBy(Model $user)
    {
        if (\is_a($user, \config('auth.providers.users.model'))) {
            if ($this->relationLoaded('reporters')) {
                return $this->reporters->contains($user);
            }

            return tap($this->relationLoaded('feedback') ? $this->feedback : $this->feedback())
                ->where(\config('feedback.user_foreign_key', 'user_id'), $user->getKey())->count() > 0;
        }

        return false;
    }

    /**
     * Attach a feedback to this model.
     *
     * @param string $feedback
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function receiveFeedback(string $feedback, string $type = null)
    {
        return $this->receiveFeedbackFromUser(auth()->user(), $feedback, $type);
    }

    /**
     * Attach a feedback to this model as a specific user.
     *
     * @param Model|null $user
     * @param string $feedback
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function receiveFeedbackFromUser(?Model $user, string $feedback, string $type = null)
    {
        $feedbackClass = config('feedback.feedback_class');

        $feedback = new $feedbackClass([
            'content' => $feedback,
            'type' => $type ?? Feedback::FLAG_DEFAULT,
            'user_id' => is_null($user) ? null : $user->getKey(),
            'reportable_id' => $this->getKey(),
            'reportable_type' => get_class(),
        ]);

        return $this->feedback()->save($feedback);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function feedback()
    {
        return $this->morphMany(\config('feedback.feedback_class'), 'reportable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reporters()
    {
        return $this->belongsToMany(
            config('auth.providers.users.model'),
            config('feedback.table'),
            'reportable_id',
            config('feedback.user_foreign_key')
        )
            ->where('reportable_type', $this->getMorphClass());
    }
}