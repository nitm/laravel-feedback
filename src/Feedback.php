<?php

namespace Nitm\Feedback;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Nitm\Feedback\Contracts\Feedback as FeedbackContract;

/**
 * @SWG\Definition(
 *      definition="Feedback",
 *      required={"reportable_type", "reportable_id", "content", "type"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="reportable_type",
 *          description="reportable_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="reportable_id",
 *          description="reportable_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Feedback extends Model implements FeedbackContract
{
    use SoftDeletes, HasFactory;

    public $table = 'feedback';

    // Feedback flags
    const FLAG_DEFAULT       = 'feedback';
    const FLAG_INAPPROPRIATE = 'inappropriate';

    public $fillable = [
        'reportable_type',
        'reportable_id',
        'content',
        'user_id',
        'type',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'              => 'integer',
        'reportable_type' => 'string',
        'reportable_id'   => 'integer',
        'content'         => 'string',
        'user_id'         => 'integer',
        'type'            => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'reportable_type' => 'required|string|max:255',
        'reportable_id'   => 'required',
        'content'         => 'required|string',
        'user_id'         => 'nullable',
        'type'            => 'sometimes|string',
        'created_at'      => 'nullable',
        'updated_at'      => 'nullable',
        'deleted_at'      => 'nullable',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function reportable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo($this->getAuthModelName(), 'user_id');
    }

    /**
     * Get the auth model class
     *
     * @return void
     */
    protected function getAuthModelName()
    {
        if (!is_null($class = config('feedback.user_model'))) {
            return $class;
        }

        if (!is_null($class = config('auth.providers.users.model'))) {
            return $class;
        }

        throw new Exception('Could not determine the feedback user model name.');
    }

    /**
     * Get Types
     *
     * @return array
     */
    public function getTypes(): array
    {
        return [
            static::FLAG_DEFAULT       => Str::title(static::FLAG_DEFAULT),
            static::FLAG_INAPPROPRIATE => Str::title(static::FLAG_INAPPROPRIATE),
        ];
    }
}
